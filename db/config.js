const Mongoose  = require("mongoose");

const dbConnection = async ( ) => {

    try {
        
        Mongoose.connect(process.env.BD_CNN, {
            // useNewUrlParser: true,
            // useUnifiedTopology: true,
            // useCreatedIndex: true
        });

        console.log('BD Online');

    } catch (error) {
        console.log(error);
        throw new Error('Error a la hora de iniciar la Base de Datos');
    }

}

module.exports = {
    dbConnection
}