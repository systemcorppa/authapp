const { response } = require('express');
const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');


const crearUsuario = async (req, resp = response) => {

    const { email, name, password } = req.body;

    try {
        // verificar email
        const usuario = await Usuario.findOne({ email });

        if (usuario) {
            return resp.status(400).json({
                ok: false,
                msg: 'Usuario ya existe con ese email'
            });
        }

        //crear usuario con el modelo
        const dbUser = new Usuario(req.body);

        //hash la contraseña
        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync(password, salt);

        //generar JWT
        const token = await generarJWT(dbUser.id, name);

        //Crear usuario BD
        await dbUser.save();

        // Generar respuesta exitosa 
        return resp.status(201).json({
            ok: true,
            uid: dbUser.id,
            name,
            email,
            token
        });


    } catch (error) {
        console.log(error);
        return resp.status(500).json({
            ok: false,
            msg: 'Porfavor hablar con el Administrador'
        })
    }

}


const loginUsuario = async (req, resp = response) => {

    const { email, password } = req.body;

    try {

        const dbUser = await Usuario.findOne({ email });

        if ( !dbUser ) {
            return resp.status(400).json({
                ok: false,
                msg: 'El correo no existe'
            });
        }

        // confirmar si el password hace match
        const validPassword = bcrypt.compareSync( password, dbUser.password);

        if (!validPassword) {
            return resp.status(400).json({
                ok: false,
                msg: 'Contraseña no coincide'
            }); 
        }

        //Generar JWT
        const token = await generarJWT(dbUser.id, dbUser.name);

        // respuesta del servicio
        return resp.json({
            ok: true,
            uid: dbUser.id,
            name: dbUser.name,
            email: dbUser.email,
            token
        });

        
    } catch (error) {
        console.log(err)
        return resp.status(500).json({
            ok: false,
            msg: 'Contacte al Administrador'
        });
    }

}

const revalidarToken = async (req, resp = response) => {

    const { uid } = req;
    
    //Leer la BD 
    const dbUser = await Usuario.findById ( uid );

    

    //generar el JWT
    const token = await generarJWT( uid, dbUser.name);

    return resp.json({
        ok: true,
        uid,
        name: dbUser.name,
        email: dbUser.email,
        token        
    })

}

module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}
